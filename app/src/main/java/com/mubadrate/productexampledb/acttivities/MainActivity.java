package com.mubadrate.productexampledb.acttivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mubadrate.productexampledb.R;
import com.mubadrate.productexampledb.db.ProductDatabase;
import com.mubadrate.productexampledb.db.User;

public class MainActivity extends AppCompatActivity {

    Button login,register;
    EditText userName,password;
    ProductDatabase productDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.loginBtn);
        register = findViewById(R.id.registerbtn);
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.PasswordEt);
        productDatabase = ProductDatabase.getInstance(this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            User user= productDatabase.userDAO().LoginWithUser(userName.getText().toString(),password.getText().toString());
            if(user!=null){
                Intent i = new Intent(MainActivity.this,ProductActivity.class);
                startActivity(i);
            }else{
                Toast.makeText(MainActivity.this, "Incorrect User", Toast.LENGTH_SHORT).show();
            }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,RegistrationActivity.class);
                startActivity(i);

            }
        });
    }
}