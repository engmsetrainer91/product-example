package com.mubadrate.productexampledb.acttivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mubadrate.productexampledb.R;
import com.mubadrate.productexampledb.db.Product;
import com.mubadrate.productexampledb.db.ProductDatabase;

public class ProductActivity extends AppCompatActivity {

    EditText id,productName,productQnty,productPrice;
    Button saveProductBtn,showAllProduct;
    ProductDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        id = findViewById(R.id.productId);
        productName = findViewById(R.id.productName);
        productQnty = findViewById(R.id.productQnty);
        productPrice = findViewById(R.id.productPrice);
        saveProductBtn = findViewById(R.id.saveProductBtn);
        showAllProduct= findViewById(R.id.showAllProduct);
        database = ProductDatabase.getInstance(this);
        saveProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idp = Integer.parseInt(id.getText().toString());
                String name =   productName.getText().toString();
                double price = Double.parseDouble(productPrice.getText().toString());
                int qnty = Integer.parseInt(productQnty.getText().toString());
                Product p = new Product(idp,qnty,name,price);
                database.productDAO().insertProduct(p);
                Toast.makeText(ProductActivity.this, "Product Added Successfully", Toast.LENGTH_SHORT).show();
            }
        });

        showAllProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(ProductActivity.this,ShowAllProductActivity.class));
            }
        });

    }
}