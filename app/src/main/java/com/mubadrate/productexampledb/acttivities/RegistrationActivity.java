package com.mubadrate.productexampledb.acttivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mubadrate.productexampledb.R;
import com.mubadrate.productexampledb.db.ProductDatabase;
import com.mubadrate.productexampledb.db.User;

public class RegistrationActivity extends AppCompatActivity {

    EditText userName,password,cpassword,email;
    Button register;
    ProductDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        register = findViewById(R.id.registerbtn);
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.PasswordEt);
        cpassword = findViewById(R.id.PasswordConfirmEt);
        email = findViewById(R.id.email);
        database = ProductDatabase.getInstance(this);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(userName.getText().toString(),email.getText().toString(),password.getText().toString());
                database.userDAO().insertUser(user);
                Toast.makeText(RegistrationActivity.this, "User Inserted Successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }
}