package com.mubadrate.productexampledb.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Products")
public class Product {
    @PrimaryKey
    @ColumnInfo(name = "id")
    int id;
    @ColumnInfo(name = "Qnty")
    int qnty;
    @ColumnInfo(name = "product_name")
    String name;
    @ColumnInfo(name = "Price")
    double price;

    public Product(int id, int qnty, String name, double price) {
        this.id = id;
        this.qnty = qnty;
        this.name = name;
        this.price = price;
    }
}
