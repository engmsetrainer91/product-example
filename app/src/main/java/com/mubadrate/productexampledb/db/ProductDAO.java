package com.mubadrate.productexampledb.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface ProductDAO {
    @Insert
    void insertProduct(Product product);

    @Delete
    void deleteProduct(Product product);

    @Query("select * from Products")
    List<Product> getAllProducts();
}
