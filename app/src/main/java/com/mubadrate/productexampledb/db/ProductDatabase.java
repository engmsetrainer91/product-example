package com.mubadrate.productexampledb.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {User.class,Product.class} , version = 2)
public abstract class ProductDatabase extends RoomDatabase {
 public static final String db_name = "PRODUCTDB";
 public static  ProductDatabase productDatabase;
 public static  synchronized   ProductDatabase getInstance(Context context){
     if(productDatabase==null){

         productDatabase = Room.databaseBuilder(context,ProductDatabase.class,db_name).
                 fallbackToDestructiveMigration().
                 allowMainThreadQueries().
                 build();
         return productDatabase;
     }
     return productDatabase;
 }

 public abstract UserDAO userDAO();

 public abstract ProductDAO productDAO();

}
