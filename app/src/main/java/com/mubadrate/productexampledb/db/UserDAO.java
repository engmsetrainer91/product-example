package com.mubadrate.productexampledb.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
@Dao
public interface UserDAO {

    @Insert
    void insertUser(User user);

    @Delete
    void deleteUser(User user);

    @Query("select * from Users ")
    List<User>  getAllUser();


    @Query("select * from Users where user_name=:uname and user_password=:password")
    User LoginWithUser(String uname,String password);
}
